﻿
using System.Collections.Generic;


namespace Entities.Abstracs
{
    public class QuestionBaseEntity
    {
        public TopicBaseEntity topic = new TopicBaseEntity();

        public string Title { get; set; }
        public string RightAnswer { get; set; }
        

        public List<string> Answers = new List<string>();

        public QuestionBaseEntity()
        {
        }

        public TopicBaseEntity Topic
        {
            get
            {
                return topic;
            }
            set
            {
                topic = value;
            }
        }

    }
}
