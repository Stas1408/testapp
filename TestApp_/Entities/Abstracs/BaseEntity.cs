﻿

namespace Entities.Abstracs
{
    public abstract class BaseEntity : IBaseEntity
    {
        public int Id { get; set; }
    }
}
