﻿using Entities.Abstracs;
using System.Collections.Generic;

namespace Entities
{
    public class TestEntity : BaseEntity
    {
        private List<QuestionBaseEntity> test = new List<QuestionBaseEntity>();

        private List<TopicBaseEntity> topics = new List<TopicBaseEntity>();

        public TestEntity()
        {
        }

        public List<QuestionBaseEntity> Test
        {
            get
            {
                return test;
            }
            set
            {
                test = value;
            }
        }

        public List<TopicBaseEntity> Topics
        {
            get
            {
                return topics;
            }
            set
            {
                topics = value;
            }
        }
    }
}
