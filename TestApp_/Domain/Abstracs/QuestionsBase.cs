﻿

namespace Domain.Abstracs
{
    public abstract class QuestionsBase
    {
        public string Title { get; set; }
        public string RightAnswer { get; set; }

        public TopicBase Topic { get; set;  }

    }
}
