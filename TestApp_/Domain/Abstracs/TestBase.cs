﻿
using System.Collections.Generic;

namespace Domain.Abstracs
{
    public class TestBase
    {
        public List<QuestionsBase> Questions = new List<QuestionsBase>();
        public List<TopicBase> Topics = new List<TopicBase>();
    }
}
