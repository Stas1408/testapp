﻿using Domain.Abstracs;
using System;
using System.Collections.Generic;

namespace Domain
{
    public class QuestionOpen:QuestionsBase
    {
        public List<string> Answers = new List<string>();

        public QuestionOpen(List<string> answers,string Title,String RightAnswer)
        {
            Answers = answers;
            base.RightAnswer = RightAnswer;
            base.Title = Title;
        }

        public QuestionOpen(List<string> answers, string Title)
        {
            Answers = answers;
            base.Title = Title;
        }

        public QuestionOpen()
        {
        }
    }
}
