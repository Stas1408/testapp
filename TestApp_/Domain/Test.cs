﻿using Domain.Abstracs;
using System.Collections.Generic;

namespace Domain
{
    public class Test : TestBase
    {
        public Test()
        {
        }

        public Test(List<TopicBase> topics, List<QuestionsBase> questions)
        {
            base.Questions = questions;
            base.Topics = topics;
        }
    }
}
