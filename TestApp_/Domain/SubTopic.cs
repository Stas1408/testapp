﻿using Domain.Abstracs;

namespace Domain
{
    public class SubTopic:TopicBase
    {
        public SubTopic(string Name)
        {
            base.Name = Name;
        }
    }
}
