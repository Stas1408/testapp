﻿using Domain.Abstracs;

namespace Domain
{
    public class SubSubTopic : TopicBase
    {
        public SubSubTopic(string Name)
        {
            base.Name = Name;
        }
    }
}
