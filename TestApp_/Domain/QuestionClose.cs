﻿using Domain.Abstracs;
using System;

namespace Domain
{
    public class QuestionClose : QuestionsBase
    {
        public QuestionClose()
        {
        }

        public QuestionClose(string Title, String RightAnswer)
        {
            base.RightAnswer = RightAnswer;
            base.Title = Title;
        }

    }
}
