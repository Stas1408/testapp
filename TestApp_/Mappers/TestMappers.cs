﻿using Domain;
using Domain.Abstracs;
using Entities;
using Entities.Abstracs;
using System.Collections.Generic;


namespace Mappers
{
    public static class TestMappers
    {
        public static TestEntity ToEntity(this TestBase test)
        {

            return new TestEntity
            {
                Test = ListToQuestionBaseEntities(test.Questions),
                Topics=ListToTopicBaseEntities(test.Topics)
            };
        }

        public static List<QuestionBaseEntity> ListToQuestionBaseEntities(List<QuestionsBase> _list)
        {
            List<QuestionBaseEntity> list = new List<QuestionBaseEntity>();
            foreach(QuestionsBase questionsBase in _list)
            {
                QuestionBaseEntity questionBaseEntity = new QuestionBaseEntity();
                questionBaseEntity.RightAnswer = questionsBase.RightAnswer;
                questionBaseEntity.Title = questionsBase.Title;
                questionBaseEntity.Topic.Name = questionsBase.Topic.Name;
                
                if (questionsBase is QuestionOpen)
                {
                    QuestionOpen questionOpen =(QuestionOpen)questionsBase;
                    questionBaseEntity.Answers = questionOpen.Answers;
                }
                else
                {
                    questionBaseEntity.Answers = null;
                }
                list.Add(questionBaseEntity);
            }
            return list;
        }

        public static List<TopicBaseEntity> ListToTopicBaseEntities(List<TopicBase> topics)
        {
            List<TopicBaseEntity> topicsEntity = new List<TopicBaseEntity>();
            foreach(TopicBase topicBase in topics)
            {
                topicsEntity.Add(new TopicBaseEntity() {Name=topicBase.Name });
            }
            return topicsEntity;
        }


        public static Test ToTestBase(this TestEntity testEntiy)
        {
            return new Test()
            {
                Questions=ListToQuestionBase(testEntiy.Test),
                Topics = ListToTopicBase(testEntiy.Topics)

            };

        }

        public static List<QuestionsBase> ListToQuestionBase(List<QuestionBaseEntity> _list)
        {
            List<QuestionsBase> list = new List<QuestionsBase>();
            foreach (QuestionBaseEntity questionsBaseEntity in _list)
            {
                if(questionsBaseEntity.Answers==null)
                {
                    list.Add( new QuestionClose() { RightAnswer = questionsBaseEntity.RightAnswer, Title = questionsBaseEntity.Title, Topic = new Topic(questionsBaseEntity.Topic.Name) });
                }
                else
                {
                   list.Add(  new QuestionOpen() { RightAnswer = questionsBaseEntity.RightAnswer, Title = questionsBaseEntity.Title, Topic = new Topic(questionsBaseEntity.Topic.Name),Answers=questionsBaseEntity.Answers });
                }
                
            }
            return list;
        }

        public static List<TopicBase> ListToTopicBase(List<TopicBaseEntity> topicsEntity)
        {
            List<TopicBase> topics = new List<TopicBase>();
            foreach (TopicBaseEntity topicBaseEntity in topicsEntity)
            {
                topics.Add(new Topic(topicBaseEntity.Name));
            }
            return topics;
        }
    }
}
