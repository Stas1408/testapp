﻿using Data.Repositores;
using Domain;
using Domain.Abstracs;
using Services;
using Services.Abstracts;
using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;

namespace Ui
{
    public class ConsoleApplication
    {
        private readonly ITestService service;

        public ConsoleApplication(ITestService service)
        {
            this.service = service;
        }

        public void SayHello()
        {

            Console.WriteLine("\t\tДобро пожаловать в программу для работы с тестами\n" +
               "\tЗдесь Вы сможете проходить, добавлять и редактировать тесты\n" +
               "Нажмите любую клавишу, чтобы продолжить...");

            Console.ReadKey();

            while (true)
            {
                RenderHeader();
                CheckButton();
            }

        }

        public void RenderHeader()
        {
            Console.Clear();

            Console.WriteLine("Нажмите один из символов для совершения операции:\n" +
                "\t1 - Добавить тест\n" +
                "\t2 - Пройти тест\n" +
                "\t3 - Редактировать тест\n");
        }

        public void CheckButton()
        {

                
                ConsoleKeyInfo button = Console.ReadKey();

                if (button.Key == ConsoleKey.NumPad1 || button.KeyChar == '1')
                {
                    MakeTest();
                }
                else if (button.Key == ConsoleKey.NumPad2 || button.KeyChar == '2')
                {
                    Test test = ChooseTest();
                    (List<QuestionsBase>,double) result = TestService.GetStatistics(StartTest(test),test);
                    PrintStatistics(result.Item1, result.Item2);
                }
                else if (button.Key == ConsoleKey.NumPad2 || button.KeyChar == '3')
                {
                    EditTest(ChooseTest());
                }




        }

        public void MakeTest()
        {

            List<TopicBase> topics = MakeTopics();
            List<QuestionsBase> questions = MakeQuestions(topics);
            service.AddTest(new Test(topics, questions));
        }

        public List<TopicBase> MakeTopics()
        {
            List<TopicBase> topics = new List<TopicBase>();
            Console.WriteLine("Введите тему теста:");
            string topicName = CheckTopicsName(Console.ReadLine());
            topics.Add(new Topic(topicName));

            bool CheckaddTopics = true;
            while (CheckaddTopics)
            {

                Console.WriteLine("Желаете добавить подтему");
                Console.WriteLine("Нажмите один из символов для совершения операции:\n" +
                    "\t1 - Да\n" +
                    "\t2 - Нет\n");
                ConsoleKeyInfo button = Console.ReadKey();

                if (button.Key == ConsoleKey.NumPad1 || button.KeyChar == '1')
                {
                    Console.WriteLine("Введите подтему теста:");
                    string subtopicName = CheckTopicsName(Console.ReadLine());
                    topics.Add(new SubTopic(subtopicName));
                }
                else if (button.Key == ConsoleKey.NumPad2 || button.KeyChar == '2')
                {
                    CheckaddTopics = false;
                }
            }
            return topics;
        }

        public static string CheckTopicsName(string Name)
        {
            while (Name.Length >= 30)
            {
                Console.WriteLine("Название слишком длинное\nТема теста: ");
                Name = Console.ReadLine();
            }
            while (Name.Length <= 4)
            {
                Console.WriteLine("Название слишком короткое\nТема теста: ");
                Name = Console.ReadLine();
            }
            return Name;
        }

        public List<QuestionsBase> MakeQuestions(List<TopicBase> topics)
        {
            List<QuestionsBase> test = new List<QuestionsBase>();

            bool CheckAddQuestions = true;
            while (CheckAddQuestions)
            {

                Console.WriteLine("Желаете добавить вопрос?");
                Console.WriteLine("Нажмите один из символов для совершения опрерации:\n" +
                    "\t1 - Да\n" +
                    "\t2 - Нет\n");
                ConsoleKeyInfo button = Console.ReadKey();

                if (button.Key == ConsoleKey.NumPad1 || button.KeyChar == '1')
                {

                    Console.WriteLine("Какой желаете добавить вопрос?");
                    Console.WriteLine("Нажмите один из символов для совершения опрерации:\n" +
                        "\t1 - Открытый\n" +
                        "\t2 - Закрытый\n");
                    ConsoleKeyInfo button1 = Console.ReadKey();

                    if (button1.Key == ConsoleKey.NumPad1 || button1.KeyChar == '1')
                    {
                        test.Add(MakeOpenQuestion(topics));
                    }
                    else if (button1.Key == ConsoleKey.NumPad2 || button1.KeyChar == '2')
                    {
                        test.Add(MakeCloseQuestion(topics));
                    }

                    Console.WriteLine($"\nДля возобновления работы программы нажмите любую клавишу...");
                    Console.ReadKey();

                }
                else if (button.Key == ConsoleKey.NumPad2 || button.KeyChar == '2')
                {
                    CheckAddQuestions = false;
                }
            }

            return test;
        }

        public QuestionOpen MakeOpenQuestion(List<TopicBase> topics)
        {
            Console.WriteLine("Введите вопрос");
            string title = Console.ReadLine();
            List<string> answers = new List<string>();

            while (true)
            {
                Console.WriteLine("Введите ответ");
                answers.Add(Console.ReadLine());
                Console.WriteLine("Желаете добавить еще один ответ?");

                Console.WriteLine("Нажмите один из символов для совершения опрерации:\n" +
                        "\t1 - Да\n" +
                        "\t2 - Нет\n");

                ConsoleKeyInfo button = Console.ReadKey();
                if (button.Key == ConsoleKey.NumPad1 || button.KeyChar == '1')
                {

                }
                else if (button.Key == ConsoleKey.NumPad2 || button.KeyChar == '2')
                {
                    break;
                }

            }
            QuestionOpen questionOpen = new QuestionOpen(answers, title);
            PrintQuestion(questionOpen);
            Console.WriteLine("Введите номер правильного ответа");
            questionOpen.RightAnswer = CheckCountInList(answers);

            questionOpen.Topic = ChooseQuestionTopic(topics);

            return questionOpen;
        }

        public static string CheckCountInList(List<string> answers)
        {
            int numAnswers;
            while (!Int32.TryParse(Console.ReadLine(), out numAnswers) || numAnswers <= 0 || numAnswers > answers.Count)
            {
                Console.WriteLine("Некорректный ввод");
            }
            return numAnswers.ToString();
        }

        public QuestionClose MakeCloseQuestion(List<TopicBase> topics)
        {
            Console.WriteLine("Введите вопрос");
            string title = Console.ReadLine();
            Console.WriteLine("Введите правильный ответ");
            string rightAnswer = Console.ReadLine();
            QuestionClose questionClose = new QuestionClose(title, rightAnswer);
            questionClose.Topic = ChooseQuestionTopic(topics);
            PrintQuestion(questionClose);
            return questionClose;


        }

        public static void PrintQuestion(QuestionsBase question)
        {
            Console.WriteLine(question.Title);
            if (question is QuestionOpen)
            {
                QuestionOpen questionOpen = (QuestionOpen)question;
                int i = 1;
                foreach (string answer in questionOpen.Answers)
                {
                    Console.WriteLine(i + ")\t" + answer);
                    i++;
                }
            }
        }

        public TopicBase ChooseQuestionTopic(List<TopicBase> topics)
        {
            Console.WriteLine("Выберете тему к которой относится тест");
            int i = 1;
            foreach (TopicBase topic in topics)
            {
                Console.WriteLine(i + "\t" + topic.Name);
                i++;
            }
            int num;
            while (!Int32.TryParse(Console.ReadLine(), out num) || num > topics.Count || num <= 0)
            {
                Console.WriteLine("Некорректное число");
            }

            return topics[--num];
        }

        public Test ChooseTest()
        {
            Console.WriteLine("Список тестов");
            int i = 0;
            foreach (TestBase test in service.GetAllTest())
            {
                i++;
                Console.WriteLine(i + ") " + test.Topics[0]);
            }
            Console.WriteLine("Введите номер теста");
            int numAnswers;
            while (!Int32.TryParse(Console.ReadLine(), out numAnswers) || numAnswers <= 0 || numAnswers > service.GetAllTest().Count)
            {
                Console.WriteLine("Некорректный ввод");
            }
            return service.GetAllTest()[--numAnswers];
        }

        public List<string> StartTest(Test test)
        {
            List<string> answers = new List<string>();
            int i = 1;
            foreach (QuestionsBase questionBase in test.Questions)
            {
                Console.WriteLine("Вопос №"+i);
                i++;
                PrintQuestion(questionBase);
                if (questionBase is QuestionOpen)
                {
                    QuestionOpen questionOpen = (QuestionOpen)questionBase;
                    answers.Add(CheckCountInList(questionOpen.Answers));
                }
                else
                {
                    answers.Add(Console.ReadLine());
                }
            }
            return answers;
        }


        public void EditTest(Test test)
        {
            Test testEdit = new Test();

            while (true)
            {
                Console.WriteLine("Нажмите один из символов для совершения операции:\n" +
                    "\t1 - Удалить тест\n" +
                    "\t2 - Редактировать темы\n" +
                    "\t3 - Редактировать тест\n" +
                    "\t4 - Выход\n");

                ConsoleKeyInfo button = Console.ReadKey();
                if (button.Key == ConsoleKey.NumPad1 || button.KeyChar == '1')
                {
                    service.DeleteTest(test);
                }
                else if (button.Key == ConsoleKey.NumPad1 || button.KeyChar == '2')
                {
                    testEdit.Topics = MakeTopics();
                    service.AddTest(test);
                }
                else if (button.Key == ConsoleKey.NumPad1 || button.KeyChar == '3')
                {
                    testEdit.Questions = MakeQuestions(testEdit.Topics);
                    service.AddTest(test);
                }
                else if (button.Key == ConsoleKey.NumPad1 || button.KeyChar == '4')
                {
                    break;
                }
                else
                    Console.WriteLine("Некорректный ввод");
            }
        }

        public void PrintStatistics(List<QuestionsBase> questionsError, double mark)
        {
            Console.WriteLine("Итоговая оценка за тест: " + mark);
            if(questionsError.Count>0)
            {
                Console.WriteLine("Вопросы в которых допущены ошибки");
            }
            foreach (QuestionsBase question in questionsError)
            {
                Console.WriteLine("Вопрос");
                Console.WriteLine(question.Title);
                Console.WriteLine("Ответы:");
                if (question is QuestionOpen)
                {

                    QuestionOpen questionOpen = (QuestionOpen)question;
                    int i = 1;
                    foreach (string answer in questionOpen.Answers)
                    {
                        if (i.ToString() == questionOpen.RightAnswer)
                        {

                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine(i + ")\t" + answer);
                            Console.ForegroundColor = ConsoleColor.White;

                        }
                        else
                            Console.WriteLine(i + ")\t" + answer);
                        i++;

                    }
                }
                else
                {

                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine(question.RightAnswer);
                    Console.ForegroundColor = ConsoleColor.White;

                }
                Console.WriteLine("Нажмите любую клавишу для продолжения...");
                Console.ReadKey();
            }
            Console.ReadKey();
        }
    }
}
