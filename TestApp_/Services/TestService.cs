﻿
using Data.Repositores.Abstract;
using Domain;
using Domain.Abstracs;
using Entities;
using Mappers;
using Services.Abstracts;
using System.Collections.Generic;

namespace Services
{
    public class TestService : ITestService
    {
        private readonly ITestRepository _testRepository;
        public TestService(ITestRepository testRepository)
        {
            _testRepository = testRepository;
        }

        public void AddTest(Test test)
        {
            _testRepository.AddTest(TestMappers.ToEntity(test));
        }

        public void DeleteTest(Test test)
        {
            _testRepository.DeleteTest(TestMappers.ToEntity(test));
        }

        public List<Test> GetAllTest()
        {
            List<Test> tests = new List<Test>();
            foreach(TestEntity testEntity in _testRepository.GetAllTest())
            {
                tests.Add(Mappers.TestMappers.ToTestBase(testEntity));
            }
            return tests;
        }

        public static (List<QuestionsBase>,double) GetStatistics(List<string> answer, Test test)
        {
            List<QuestionsBase> questionsError = new List<QuestionsBase>();
            for(int i=0;i<test.Questions.Count;i++)
            {
                if (test.Questions[i].RightAnswer!=answer[i])
                {
                    questionsError.Add(test.Questions[i]);
                }
            }
            return (questionsError,((answer.Count-questionsError.Count) *100 / answer.Count));
        }
    }
}
