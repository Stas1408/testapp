﻿using Domain;
using System.Collections.Generic;

namespace Services.Abstracts
{
    public interface ITestService
    {
        public void AddTest(Test test);
        public List<Test> GetAllTest();
        public void DeleteTest(Test test);
    }
}
