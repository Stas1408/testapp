﻿using Data.Repositores.Abstract;
using Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;

namespace Data.Repositores
{
    public class TestRepository : ITestRepository
    {

        private readonly string filePath = System.IO.Directory.GetCurrentDirectory() + @"\Json.json";

        private List<TestEntity> _list = new List<TestEntity>();    

        public void AddTest(TestEntity testEntity)
        {
            _list.Add(testEntity);
        }

        public void DeleteTest(TestEntity testEntity)
        {
            for(int i =0; i<_list.Count;i++)
            {
                if(_list[i]==testEntity)
                {
                    _list.RemoveAt(i);
                }
            }
        }

        public List<TestEntity> GetAllTest()
        {
            return _list;
        }

        public async void SaveData()
        {

            using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate))
            {
                await JsonSerializer.SerializeAsync<List<TestEntity>>(fs, _list);

            }
           
        }

        public async void ReadData()
        {

            using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate))
            {
                _list = await JsonSerializer.DeserializeAsync<List<TestEntity>>(fs);

            }
        }

    }
}
