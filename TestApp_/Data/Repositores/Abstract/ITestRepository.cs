﻿
using Entities;
using System.Collections.Generic;

namespace Data.Repositores.Abstract
{
    public interface ITestRepository
    {
        void AddTest(TestEntity testEntity);
        List<TestEntity> GetAllTest();
        void DeleteTest(TestEntity testEntity);
    }
}
